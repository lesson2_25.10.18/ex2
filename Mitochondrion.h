#pragma once
#include "Protein.h"

class Mitochondrion
{
public:
	void init();
	void insert_glucose_receptor(const Protein & protein);
	void set_glucose(const unsigned int glocuse_units);
	bool produceATP() const; //const int glocuse_unit

private:
	unsigned int _glocuse_level;//The amount of glocuse in the Mitochondrion, when at least 50, atp can be created
	bool _has_glocuse_receptor;//Whether the Mitochondrion can collect glocuse, true if yes, otherwise false 
	Protein* testChain = new Protein;
	void createTestProtain();//Creates the test protein chain
};