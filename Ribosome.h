#pragma once
#include "Protein.h"
#include "AminoAcid.h"

class Ribosome
{
public:
	Protein* create_protein(std::string &RNA_transcript) const;
};