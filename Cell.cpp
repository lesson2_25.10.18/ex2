#include "Cell.h"

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_mitochondrion.init();
	this->_apt_units = 0;
}

bool Cell::get_ATP()
{
	std::string transcript = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);
	Protein* p = this->_ribosome.create_protein(transcript);

	if (p == nullptr)
	{
		std::cerr << "Couldn't create protein" << std::endl;
		_exit(1);
	}

	this->_mitochondrion.insert_glucose_receptor(*p);
	this->_mitochondrion.set_glucose(50);

	if (!this->_mitochondrion.produceATP())
	{
		return false;
	}
	this->_apt_units = 100;
	return true;

}