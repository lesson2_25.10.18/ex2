#include "Ribosome.h"

Protein* Ribosome::create_protein(std::string &RNA_transcript) const
{
	Protein* p = new Protein;
	std::string codon = "";
	AminoAcid acid;
	int i = 0;

	p->init();

	for (i = 0; i < (RNA_transcript.length() - 3); i += 3)
	{
		codon = RNA_transcript.substr(i, 3);

		acid = get_amino_acid(codon);
		if (acid == AminoAcid::UNKNOWN)
			p->clear();
		else
		{
			p->add(acid);
		}
	}
	return p;
}