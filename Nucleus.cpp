#include "Nucleus.h"

///////////////////
//      Gene     //
///////////////////
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this-> _on_complementary_dna_strand = on_complementary_dna_strand;
}

unsigned int Gene::get_start() const
{
	return this->_start;
}

unsigned int Gene::get_end() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}

void Gene::setStart(const unsigned int start)
{
	this->_start = start;
}

void Gene::setEnd(const unsigned int end)
{
	this->_end = end;
}

void Gene::set_on_complementary_dna_strand(const bool on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

///////////////////
//    Nucleus    //
///////////////////

void Nucleus::init(const std::string dna_sequence)
{
	std::string dna_comp;
	int i = 0;
	char curr = 0;
	for (i = 0; i < dna_sequence.length(); i++)
	{
		curr = dna_sequence[i];
		if (curr == 'A')
			dna_comp += 'T';
		else if (curr == 'C')
			dna_comp += 'G';
		else if (curr == 'G')
			dna_comp += 'C';
		else if (curr == 'T')
			dna_comp += 'A';
		else
		{
			std::cerr << "Wrong DNA sequence";
			_exit(1);
		}
	}
	this->_DNA_strand = dna_sequence;
	this->_complementary_DNA_strand = dna_comp;
}

std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	int i = 0;
	std::string rna = "";
	for (i = gene.get_start(); i <= gene.get_end(); i++)
	{
		if(!gene.is_on_complementary_dna_strand())
		{
			if (this->_DNA_strand[i] == 'T')
				rna += 'U';
			else
				rna += this->_DNA_strand[i];
		}
		else
		{
			if (this->_complementary_DNA_strand[i] == 'T')
				rna += 'U';
			else
				rna += this->_complementary_DNA_strand[i];
		}
	}

	return rna;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	int i = 0;
	std::string reversed = "";

	for (i = ((this->_DNA_strand.length()) - 1); i >= 0; i--)
	{
		reversed += this->_DNA_strand[i];
	}

	return reversed;
	
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	int i = 0;
	unsigned int reps = 0;
	std::string temp = "";
	temp += this->_DNA_strand[0];
	
	for (i = 0; i < this->_DNA_strand.length(); i++)
	{
		if (temp.compare(codon) == 0)
			reps++;

		if (i % codon.length() == 0)
			temp = this->_DNA_strand[i];
		else
			temp += this->_DNA_strand[i];

	}

	return reps;
}