#pragma once
#include <iostream>


class Gene
{
public:
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);//init function
	//Getters
	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_complementary_dna_strand() const;
	//Setters
	void setStart(const unsigned int start);
	void setEnd(const unsigned int end);
	void set_on_complementary_dna_strand(const bool on_complementary_dna_strand);
private:
	unsigned int _start;//The index of the start of the Gene
	unsigned int _end;//The index of the end of the Gene
	bool _on_complementary_dna_strand;//whether the gene is on the strand of the DNA, if yes so true, otherwise false
};

class Nucleus {
public:
	void init(const std::string dna_sequence);
	std::string get_RNA_transcript(const Gene& gene) const;
	std::string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
private:
	std::string _DNA_strand;//The strand the contains the DNA genetic data
	std::string _complementary_DNA_strand;//The complementary DNA strand
};
