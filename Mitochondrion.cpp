#include "Mitochondrion.h"

void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
	this->createTestProtain();
}

void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	AminoAcidNode* curr = protein.get_first();
	AminoAcidNode* local = this->testChain->get_first();
	int i = 0;
	int count = 0;
	for (i = 0; i < 6; i++)
	{
		if (curr != nullptr)
		{
			if (curr->get_data() != local->get_data())
			{
				count = 0;
			}
			else
				count++;
		}
	}

	if (count == 6)
	{
	this->_has_glocuse_receptor = true;
	}

}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

bool Mitochondrion::produceATP() const //const int glocuse_unit
{
	return(this->_glocuse_level >= 50 && this->_has_glocuse_receptor);
}

void Mitochondrion::createTestProtain()
{
	this->testChain->init();
	this->testChain->add(AminoAcid::ALANINE);
	this->testChain->add(AminoAcid::LEUCINE);
	this->testChain->add(AminoAcid::GLYCINE);
	this->testChain->add(AminoAcid::HISTIDINE);
	this->testChain->add(AminoAcid::LEUCINE);
	this->testChain->add(AminoAcid::PHENYLALANINE);
	this->testChain->add(AminoAcid::AMINO_CHAIN_END);
}